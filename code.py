import board
import digitalio
import audiocore
import time
from adafruit_circuitplayground import cp

# Pixels settings
cp.pixels.fill((0, 0, 0))
cp.pixels.brightness = 0.01


# Note with offset to give a timing for the game
#offset and duration in seconds format
class Note:  
    def __init__(self, offset, region, duration = 0.5):  
        self.offset = offset
        self.region = region
        self.duration = duration


#Array of notes to map the song
song = []  
song.append(Note(2.8, 1)) 
song.append(Note(0, 0, 0.1,)) 
song.append(Note(0, 0)) 
song.append(Note(0.1, 1)) 
song.append(Note(0.3, 0)) 
song.append(Note(0.1, 1)) 
song.append(Note(0.5, 0, 0.2)) 
song.append(Note(0, 0, 0.2)) 
song.append(Note(1.2, 0, 0.2)) 
song.append(Note(0, 0, 0.2)) 
song.append(Note(0.1, 1)) 
song.append(Note(0, 0, 0.1)) 
song.append(Note(0.3, 0, 0.2)) 
song.append(Note(0, 0, 0.2)) 
song.append(Note(0, 1, 0.4)) 
song.append(Note(0, 0, 0.2)) 
song.append(Note(0, 0, 0.2)) 
song.append(Note(0, 0, 0.2)) 
song.append(Note(0, 1, 0.5)) 
song.append(Note(0, 0, 0.2)) 
song.append(Note(0.9, 1, 0.5)) 
song.append(Note(0, 0, 0.2)) 
song.append(Note(0, 0, 0.2)) 
song.append(Note(1.9, 0, 0.2)) 
song.append(Note(0, 0, 0.2)) 
song.append(Note(0, 1, 0.3)) 
song.append(Note(0, 0, 0.2)) 
song.append(Note(0, 0, 0.2)) 
song.append(Note(0.7, 1, 0.3)) 
song.append(Note(0, 0, 0.2)) 

#Init score and mistakes
score = 0
mistakes = 0

#Collection of leds
REGION_LEDS = (
    (3, 2, 1, 0),  # left region
    (6, 7, 8, 9) # right region
)

#Colour of the sides
REGION_COLOR = (
    (255, 255, 0),  # yellow region
    (0, 0, 255)    # blue region
)

#Logic of lightining
def light_region(region, duration=0.5):
    interval = duration / 3
    cpt = 0
    global score
    global mistakes

    # turn the LED on
    for led in REGION_LEDS[region]:
        cp.pixels[led] = REGION_COLOR[region]
        if cpt < 3:
            time.sleep(interval)
        else:
            #Wait for input
            tmpScore = play_note(region, interval)
            if tmpScore > 0:
                score += 1
            elif tmpScore == -1:
                mistakes += 1
            else:
                pass
            print("score: ", score)
            print("mistakes: ", mistakes)
        cp.pixels[led] = (0, 0, 0)
        cpt = cpt + 1

#Input logic
def play_note(region, timeout):
    val = 0
    start_time = time.monotonic()
    while time.monotonic() - start_time < timeout:
        if cp.button_a and region == 0:
            val = 1
        elif cp.button_b and region == 1:
            val = 1
        elif cp.button_a and region == 1:
            val = -1
        elif cp.button_b and region == 0:
            val = -1
    return val

#Play song logic
def play_song(song):
    for note in song:
        time.sleep(note.offset)
        light_region(note.region, note.duration)   

cp.stop_tone()
cp._speaker_enable.value = True
with cp._audio_out(board.SPEAKER) as audio:
    wavefile = audiocore.WaveFile(open("song.wav", "rb"))
    audio.play(wavefile)
    while audio.playing:
        play_song(song)


